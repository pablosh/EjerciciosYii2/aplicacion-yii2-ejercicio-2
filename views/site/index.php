<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Consultas de Selección 2';
?>

<div class="site-index">

     <!-- Encabezado -->
    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas de Selección 2</h1>
    </div>

    <div class="body-content">

        <!-- Fila 1 -->
        <div class="row">
            
            <!-- Consulta 1 -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 1</h3>
                        <p>Número de ciclistas que hay</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta1ar'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta1dao'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div> 
            </div>
            
            <!-- Consulta 2 -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 2</h3>
                        <p>Número de ciclistas que hay del equipo Banesto</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta2ar'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta2dao'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div> 
            </div>
            
            <!-- Consulta 3 -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 3</h3>
                        <p>Edad media de los ciclistas</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta3ar'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta3dao'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div> 
            </div>
            
        </div>
        
        <!-- Fila 2 -->
        <div class="row">
            
            <!-- Consulta 4 -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 4</h3>
                        <p>La edad media de los del equipo Banesto</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta4ar'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta4dao'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div> 
            </div>
            
            <!-- Consulta 5 -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 5</h3>
                        <p>La edad media de los ciclistas por cada equipo</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta5ar'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta5dao'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div> 
            </div>
            
            <!-- Consulta 6 -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 6</h3>
                        <p>El número de ciclistas por equipo</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta6ar'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta6dao'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div> 
            </div>
            
        </div>
        
        <!-- Fila 3 -->
        <div class="row">
            
            <!-- Consulta 7 -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 7</h3>
                        <p>El número total de puertos</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta7ar'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta7dao'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div> 
            </div>
            
            <!-- Consulta 8 -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 8</h3>
                        <p>El número total de puertos mayores de 1500</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta8ar'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta8dao'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div> 
            </div>
            
            <!-- Consulta 9 -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 9</h3>
                        <p>Listar el nombre de los equipos que tengan más de 4 ciclistas</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta9ar'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta9dao'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div> 
            </div>
            
        </div>

        <!-- Fila 4 -->
        <div class="row">
            
            <!-- Consulta 10 -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 10</h3>
                        <p>Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta10ar'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta10dao'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div> 
            </div>
            
            <!-- Consulta 11 -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 11</h3>
                        <p>Indícame el número de etapas que ha ganado cada uno de los ciclistas</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta11ar'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta11dao'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div> 
            </div>
            
            <!-- Consulta 12 -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 12</h3>
                        <p>Indícame el dorsal de los ciclistas que hayan ganado más de una etapa</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta12ar'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta12dao'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div> 
            </div>
            
        </div>
        
    </div>
</div>
