<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Equipo;
use app\models\Maillot;
use app\models\Etapa;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    // Consulta 1
    // Consulta 1 Active Record
    public function actionConsulta1ar(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("COUNT(*) numero_ciclistas")
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero_ciclistas'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) numero_ciclistas FROM ciclista",
        ]);
    }
    
    // Consulta 1 DAO
    public function actionConsulta1dao(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) numero_ciclistas FROM ciclista'
       ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['numero_ciclistas'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) numero_ciclistas FROM ciclista",
        ]);
    }
    
    // Consulta 2
    // Consulta 2 Active Record
    public function actionConsulta2ar(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select('COUNT(*) numero_ciclistas')->where('nomequipo = "banesto"')
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero_ciclistas'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*) numero_ciclistas FROM ciclista WHERE nomequipo='banesto'",
        ]);
    }
    
    // Consulta 2 DAO
    public function actionConsulta2dao(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) numero_ciclistas FROM ciclista WHERE nomequipo = "banesto"'
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>["numero_ciclistas"],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*) numero_ciclistas FROM ciclista WHERE nomequipo='banesto'",
        ]);
    }
    
    // Consulta 3
    // Consulta 3 Active Record
    public function actionConsulta3ar(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select('AVG(edad) edad')
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>["edad"],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) edad FROM ciclista",
        ]);
    }
    
    // Consulta 3 DAO
    public function actionConsulta3dao() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT AVG(edad) edad FROM ciclista'
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>["edad"],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) edad FROM ciclista"
        ]);
    }
    
    public function actionConsulta4ar(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select('AVG(edad) edad')->where('nomequipo = "banesto"')
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos" => ['edad'],
            "titulo" => "Consulta 4 con Active Record",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"SELECT AVG(edad) edad FROM ciclista WHERE nomequipo = 'banesto'"
        ]);
    }
    
    public function actionConsulta4dao(){
        
        $consulta="SELECT AVG(edad) edad FROM ciclista WHERE nomequipo = 'banesto'";
        
        $dataProvider = new SqlDataProvider([
            'sql'=>$consulta
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>["edad"],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"La edad media de los del equipo banesto",
            "sql"=>$consulta
        ]);   
    }
    
    public function actionConsulta5ar() {
        $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()->select('nomequipo, AVG(edad) edad')->groupBy('nomequipo')
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>["nomequipo","edad"],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT nomequipo, AVG(edad) edad FROM ciclista GROUP BY nomequipo"
        ]);
    }
    
    public function actionConsulta5dao() {
        $query="SELECT nomequipo, AVG(edad) edad FROM ciclista GROUP BY nomequipo";
        $dataProvider = new SqlDataProvider([
            'sql'=>$query
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>["nomequipo","edad"],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>$query
        ]);
    }
    
    public function actionConsulta6ar() {
        $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()->select('nomequipo, COUNT(*) numero_ciclistas')->groupBy('nomequipo')
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>["nomequipo","numero_ciclistas"],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT nomequipo, COUNT(*) numero_ciclistas FROM ciclista GROUP BY nomequipo"
        ]);
    }
    
    public function actionConsulta6dao() {
        $query="SELECT nomequipo, COUNT(*) numero_ciclistas FROM ciclista GROUP BY nomequipo";
        $dataProvider = new SqlDataProvider([
           'sql'=>$query
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>["nomequipo","numero_ciclistas"],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>$query
        ]);
    }
    
    public function actionConsulta7ar() {
        $dataProvider = new ActiveDataProvider([
            'query'=> Puerto::find()->select('COUNT(*) total_puertos')
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>["total_puertos"],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*) total_puertos FROM puerto"
        ]);
    }
    
    public function actionConsulta7dao(){
        $query="SELECT COUNT(*) total_puertos FROM puerto";
        $dataProvider = new SqlDataProvider([
            "sql"=>$query
        ]);
        
        return $this->render("resultado",[
            "resultados" => $dataProvider,
            "campos" => ["total_puertos"],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql" => $query
        ]);
    }
    
    public function actionConsulta8ar() {
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select('COUNT(*) total_puertos')->where('altura > 1500')
        ]);
        
        return $this->render("resultado",[
            "resultados" => $dataProvider,
            "campos" => ["total_puertos"],
            "titulo" => "Consulta 8 con Active Record",
            "enunciado" => "El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*) total_puertos FROM puerto WHERE altura > 1500"
        ]);
    }
    
    public function actionConsulta8dao(){
        $query="SELECT COUNT(*) total_puertos FROM puerto WHERE altura > 1500";
        $dataProvider = new SqlDataProvider([
            'sql' => $query
        ]);
        
        return $this->render("resultado",[
            "resultados" => $dataProvider,
            "campos" => ["total_puertos"],
            "titulo" => "Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql" => $query
        ]);
    }
    
    public function actionConsulta9ar(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select('nomequipo')->groupBy('nomequipo')->having('count(*) > 4')
        ]);
        
        return $this->render("resultado",[
            "resultados" => $dataProvider,
            "campos" => ["nomequipo"],
            "titulo" => "Consulta 9 con Active Record",
            "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql" => "SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*) > 4"
        ]);
    }
    
    public function actionConsulta9dao(){
        $query = "SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*) > 4";
        $dataProvider = new SqlDataProvider([
            "sql" => $query
        ]);
        
        return $this->render("resultado",[
            "resultados" => $dataProvider,
            "campos" => ["nomequipo"],
            "titulo" => "Consulta 9 con DAO",
            "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql" => $query
        ]);
    }
    
    public function actionConsulta10ar() {
        $dataProvider = new ActiveDataProvider([
            "query" => Ciclista::find()->select('nomequipo')->where('edad BETWEEN 28 AND 32')
                ->groupBy('nomequipo')->having('COUNT(*) > 4')
        ]);
        
        return $this->render("resultado",[
            "resultados" => $dataProvider,
            "campos" => ["nomequipo"],
            "titulo" => "Consulta 10 con Active Record",
            "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql" => "SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 "
            . "GROUP BY nomequipo HAVING count(*) > 4"
        ]);
    }
    
    public function actionConsulta10dao(){
        $query = "SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 "
            . "GROUP BY nomequipo HAVING count(*) > 4";
        $dataProvider = new SqlDataProvider([
            'sql' => $query
        ]);
        
        return $this->render("resultado",[
            "resultados" => $dataProvider,
            "campos" => ["nomequipo"],
            "titulo" => "Consulta 10 con DAO",
            "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql" => $query
        ]);
    }
    
    public function actionConsulta11ar(){
        $dataProvider = new ActiveDataProvider([
            "query" => Etapa::find()->select('dorsal, COUNT(*) etapas')->groupBy('dorsal')
        ]);
        
        return $this->render("resultado",[
            "resultados" => $dataProvider,
            "campos" => ["dorsal","etapas"],
            "titulo" => "Consulta 11 con Active Record",
            "enunciado" => "Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql" => "SELECT dorsal, COUNT(*) etapas FROM etapa GROUP BY dorsal"
        ]);
    }
    
    public function actionConsulta11dao(){
        $query = "SELECT dorsal, COUNT(*) etapas FROM etapa GROUP BY dorsal";
        $dataProvider = new SqlDataProvider([
            'sql' => $query
        ]);
        
        return $this->render("resultado",[
            "resultados" => $dataProvider,
            "campos" => ["dorsal","etapas"],
            "titulo" => "Consulta 11 con DAO",
            "enunciado" => "Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql" => $query
        ]);
   }
   
   public function actionConsulta12ar(){
       $dataProvider = new ActiveDataProvider([
            "query" => Etapa::find()->select('dorsal')->groupBy('dorsal')->having('COUNT(*) > 1')
        ]); 
       
       return $this->render("resultado",[
            "resultados" => $dataProvider,
            "campos" => ["dorsal"],
            "titulo" => "Consulta 12 con Active Record",
            "enunciado" => "Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql" => "SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(*) > 1"
        ]);
   }
   
   public function actionConsulta12dao(){
        $query = "SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(*) > 1";
        $dataProvider = new SqlDataProvider([
            'sql' => $query
        ]);
        
        return $this->render("resultado",[
            "resultados" => $dataProvider,
            "campos" => ["dorsal"],
            "titulo" => "Consulta 12 con DAO",
            "enunciado" => "Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql" => $query
        ]);
   }
}
